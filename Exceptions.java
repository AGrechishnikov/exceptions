import java.util.Scanner;
import java.io.*;

public class Exceptions {


    public static void main(String[] args) {
        try{

            FileReader reader = new FileReader("C:\\text.txt");

            BufferedReader bf = new BufferedReader(reader);
            String txt = null;
            try {
                txt = bf.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String[] words=txt.replaceAll("[^A-Za-z�-��-�\s]","").split(" ");
            for(String word: words){
                try{
                    if(word.length()>10){
                        throw new IndexOutOfBoundsException();
                    }
                }
                catch (IndexOutOfBoundsException e){
                    System.out.println(word + " ��������� 10 ��������!");
                }
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("file isn't found!");
        }


        Scanner scan = new Scanner(System.in);
        String consoleWords= scan.nextLine();
        String consoleWord[]=consoleWords.replaceAll("[^A-Za-z�-��-�\s]","").split(" ");
        for(String consWord: consoleWord){
            try {
                if(consWord.length()>10) throw new IndexOutOfBoundsException();
            }
            catch (IndexOutOfBoundsException e){
                System.out.println(consWord + " ��������� 10 ��������!");
            }
        }

    }

}
